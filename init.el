;; -*- lexical-binding: t; -*-

;; Paths
(defvar da/org-folder-path "~/org/")
(defvar da/vault-folder-path "~/Vault/")
(defvar da/vault-silos-extra '())

;; Allow for system local overrides
(when (file-exists-p "~/.emacs.d/sys-locals.el")
  (load-file "~/.emacs.d/sys-locals.el"))

;; User info
(setq user-full-name "Daniel Arnqvist")
(setq user-mail-address "daniel.arnqvist@gmail.com")

;; Add Melpa to packages
(with-eval-after-load 'package
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t))

(use-package emacs
  :preface
  (defun mouse-find-definitions ()
    (interactive)
    (call-interactively 'mouse-set-point)
    (call-interactively 'xref-find-definitions))

  :init
  ;; Native compilation settings
  (when (featurep 'native-compile)
    (setq native-comp-async-report-warnings-errors nil
          native-comp-jit-compilation t))

  ;; Keep custom out of init
  (setq custom-file (concat user-emacs-directory "custom.el"))
  (when (file-exists-p custom-file)
    (load custom-file))

  ;; Don't just delete things
  (setq-default delete-by-moving-to-trash t)

  :config
  (electric-pair-mode 1)
  (repeat-mode)
  (recentf-mode 1)

  (menu-bar-mode 0)
  (scroll-bar-mode 0)
  (tool-bar-mode 0)
  (blink-cursor-mode 0)
  (global-hl-line-mode 1)

  (prefer-coding-system 'utf-8)

  (setq-default indent-tabs-mode nil)
  (setq-default tab-width 4)

  :custom
  (set-mark-command-repeat-pop t)

  (create-lockfiles nil)
  (make-backup-files nil)

  (scroll-step 1)
  (scroll-conservatively 0)
  (scroll-margin 2)
  (next-screen-context-lines 5)

  (switch-to-buffer-obey-display-actions t)
  (split-height-threshold 160)
  (split-width-threshold 190)

  (inhibit-startup-screen t)
  (use-short-answers t)

  :bind
  (("M-SPC" . cycle-spacing)
   ("M-u" . upcase-dwim)
   ("M-l" . downcase-dwim)
   ("M-c" . capitalize-dwim)
   ("C-x k" . kill-current-buffer)
   ("C-x C-r" . recentf)
   ("C-z" . repeat)
   ("C-M-z" . zap-up-to-char)
   ("C-=" . hippie-expand)
   ("M-<down-mouse-1>" . mouse-find-definitions)
   ("M-<mouse-3>" . xref-go-back)))

(use-package use-package
  :custom
  (use-package-enable-imenu-support t))

(use-package ef-themes
  :ensure t
  :config
  (load-theme 'ef-dream t))

(use-package doom-modeline
  :ensure t
  :config
  (doom-modeline-mode))

(use-package isearch
  :preface
  (defun isearch-mark-exit ()
    (interactive)
    (isearch-exit)
    (push-mark isearch-other-end nil t))
  :bind (:map isearch-mode-map
         ("C-<return>" . isearch-mark-exit)))

(use-package vertico
  :ensure t
  :config
  (vertico-mode))

(use-package consult
  :ensure t
  :bind
  ;; C-x bindings (ctl-x-map)
  ("C-x M-:" . consult-complex-command)
  ("C-x b" . consult-buffer)
  ("C-x 4 b" . consult-buffer-other-window)
  ("C-x 5 b" . consult-buffer-other-frame)
  ("C-x r b" . consult-bookmark)
  ("C-x p b" . consult-project-buffer)
  ("C-x C-k C-k" . consult-kmacro)
  ;; Other custom bindings
  ("M-y" . consult-yank-pop)
  ;; M-g bindings (goto-map)
  ("M-g e" . consult-compile-error)
  ("M-g o" . consult-outline)
  ("M-g M-SPC" . consult-mark)
  ("M-g C-M-SPC" . consult-global-mark)
  ("M-g i" . consult-imenu)
  ("M-g I" . consult-imenu-multi)
  ("M-g f" . consult-flymake)
  ;; M-s bindings (search-map)
  ("M-s d" . consult-find)
  ("M-s D" . consult-locate)
  ("M-s g" . consult-grep)
  ("M-s G" . consult-git-grep)
  ("M-s r" . consult-ripgrep)
  ("M-s l" . consult-line)
  ("M-s L" . consult-line-multi)
  ("M-s m" . consult-multi-occur)
  ("M-s k" . consult-keep-lines)
  ("M-s u" . consult-focus-lines)
  ("M-s i" . consult-info)
  ;; Isearch integration
  ("M-s e" . consult-isearch-history))

(use-package embark-consult
  :ensure t
  :hook (embark-collect-mode . consult-preview-at-point-mode))

(use-package orderless
  :ensure t
  :custom
  (completion-styles '(orderless basic))
  (completion-category-overrides '(file (styles basic partial-completion))))

(use-package completion-preview
  :preface
  (require 'dabbrev)
  (defun da/dabbrev-capf ()
    (dabbrev--reset-global-variables)
    (let ((inhibit-message t))
      (ignore-errors
        (dabbrev-capf))))
  :bind (:map completion-preview-active-mode-map
              ("<backtab>" . completion-preview-complete)
              ("C-," . completion-preview-prev-candidate)
              ("C-." . completion-preview-next-candidate))
  :hook ((after-init . global-completion-preview-mode)
         (text-mode . (lambda ()
                        (add-to-list 'completion-at-point-functions 'da/dabbrev-capf))))
  :custom
  (tab-always-indent 'complete)
  (completion-in-region-function #'consult-completion-in-region)
  :config
  (setq completion-preview-completion-styles '(orderless basic))
  (add-to-list 'completion-preview-commands 'org-self-insert-command)
  (add-to-list 'completion-preview-commands 'org-delete-backward-char)
  (setf (alist-get 'completion-preview-active-mode
                   minor-mode-overriding-map-alist)
        completion-preview-active-mode-map)
  (define-advice completion-at-point (:before ())
    (completion-preview-hide)))

(use-package savehist
  :init
  (savehist-mode))

(use-package embark
  :ensure t
  :bind (("C-." . embark-act)
         ("C-c ." . embark-act-all)
         ("C-h B" . embark-bindings)
         :map embark-general-map
         ("O" . other-window-prefix))
  :custom
  (embark-indicators '(embark-minimal-indicator
                       embark-highlight-indicator
                       embark-isearch-highlight-indicator))
  (prefix-help-command #'embark-prefix-help-command)
  :config
  (add-to-list 'embark-repeat-actions 'other-window-prefix))

(use-package ibuffer
  :preface
  (defun project-ibuffer ()
  (interactive)
  (let ((project (project-current t)))
    (ibuffer nil (concat "*" (project-name project) "-ibuffer*") `((filename . ,(string-replace "~" "" (project-root project)))))))
  :bind (("C-x C-b" . ibuffer)
         ([remap project-list-buffers] . project-ibuffe)))

(use-package vundo
  :ensure t
  :bind ("C-x C-U" . vundo))

(use-package expreg
  :ensure t
  :bind ("C-," . expreg-expand)
  :config
  (defvar-keymap expreg-repeat-map
    :repeat t
    "C-," 'expreg-expand
    "C-;" 'expreg-contract))

(use-package iedit
  :ensure t)

(use-package wgrep
  :ensure t)

(use-package edit-indirect
  :ensure t
  :bind ("C-c e i" . edit-indirect-region))

(use-package rg
  :ensure t
  :bind (("M-s M-r" . rg)
         ("M-s C-M-r" . rg-menu)
         ("C-x p r" . rg-project)))

(use-package dired
  :custom
  (dired-listing-switches "-agho --group-directories-first")
  (dired-kill-when-opening-new-dired-buffer t)
  (dired-dwim-target 'dired-dwim-target-next)
  :bind (:map dired-mode-map
              ("b" . dired-up-directory)
              ("C-c C-p" . wdired-change-to-wdired-mode)))

(use-package project
  :config
  (setq project-switch-commands '((project-find-file "Find file")
                                  (rg-project "Ripgrep" ?r)
                                  (project-find-dir "Find directory")
                                  (magit-project-status "Magit" ?m)
                                  (project-ibuffer "Buffer" ?b)
                                  (project-shell "Shell")
                                  (project-eshell "Eshell")
                                  (eat-project "Eat" ?t))))


;;; Org-mode

(use-package org
  :preface
  (defun da/org-mode-setup ()
    (org-indent-mode)
    (visual-line-mode 1)
    (setq-local sentence-end-double-space nil))
  :hook ((org-mode . da/org-mode-setup))
  :bind (("C-c o c" . org-capture)
         ("C-c o a" . org-agenda)
         ("C-c o n" . org-cycle-agenda-files)
         :map org-mode-map
         ("C-," . nil)
         ("M-n" . org-next-link)
         ("M-p" . org-previous-link)
         ("C-M-RET" . org-insert-todo-heading)
         ("C-c M-RET" . org-insert-subheading))
  :custom
  (org-use-speed-commands t)
  (org-ellipsis " ▾")
  (org-return-follows-link t)
  (org-startup-folded 'showeverything)
  (org-directory da/org-folder-path)

  (org-return-follows-link t)
  (org-deadline-warning-days 14)
  (org-agenda-tags-column 75)

  (org-refile-use-outline-path 'file)
  (org-refile-allow-creating-parent-nodes 'confirm)
  (org-outline-path-complete-in-steps nil)
  (org-refile-targets '(("projects.org" :maxlevel . 1)
                        ("tickler.org" :maxlevel . 1)
                        ("someday.org" :maxlevel . 1)))

  (org-todo-keywords
   '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
     (sequence "WAIT(w@/!)" "|" "CANCELLED(k@)")))

  (org-agenda-files '("projects.org" "inbox.org" "tickler.org"))
  (org-complete-tags-always-offer-all-agenda-tags t)

  (org-capture-templates `(("n" "Note" entry (file "inbox.org")
                            "* %?" :empty-lines 1)

                           ("N" "Note - context" entry (file "inbox.org")
                            "* %?\n%U\n%a\n%i" :empty-lines 1)

                           ("t" "Task" entry (file "inbox.org")
                            "* TODO %^{Task}  %^g\n\n%?" :empty-lines 1)

                           ("T" "Task - context" entry (file "inbox.org")
                            "* TODO %^{Task}  %^g\n\n%a\n%i\n%?" :empty-lines 1)

                           ("j" "Journal" entry
                            (file+olp+datetree "journal.org")
                            "\n* %<%H:%M %p>- %^{Title}  %^g\n\n%i\n%?"
                            :empty-lines 1)))

  (org-agenda-custom-commands '(("d" "Dashboard"
                                 ((agenda "" ((org-agenda-span 14)
                                              (org-deadline-warning-days 7)))
                                  (todo "NEXT"
                                        ((org-agenda-overriding-header "Next Tasks")))
                                  (todo "TODO"
                                        ((org-agenda-files '("projects.org"))
                                         (org-agenda-overriding-header "Upcomming Tasks")))
                                  (todo "WAIT"
                                        ((org-agenda-overriding-header "Waiting Tasks")))))

                                ("n" "Next Tasks"
                                 ((todo "NEXT"
                                        ((org-agenda-overriding-header "Next Tasks")))))))
  :config
  (advice-add 'org-refile :after 'org-save-all-org-buffers))

(use-package denote
  :ensure t
  :bind (("C-c n n" . denote-open-or-create)
         ("C-c n N" . denote-silo-extras-open-or-create)
         ("C-c n c" . denote-region)
         ("C-c n d" . denote-date)
         ("C-c n s" . denote-subdirectory)
         ("C-c n t" . denote-template)
         :map dired-mode-map
         ("r" . denote-dired-rename-files))
  :custom
  (denote-known-keywords '("emacs" "programmering" "rollspel" "konst"))
  (denote-directory (expand-file-name da/vault-folder-path))
  (denote-silo-extras-directories (append (list da/vault-folder-path) da/vault-silos-extra)))

(use-package visual-fill-column
  :ensure t
  :preface
  (defun da/org-mode-visual-fill-setup ()
    (setq visual-fill-column-width 120
          visual-fill-column-center-text t)
    (visual-fill-column-mode 1))
  :hook (org-mode . da/org-mode-visual-fill-setup)
  :custom
  (visual-fill-column-enable-sensible-window-split t))


;;; Development

(use-package ediff
  :custom
  (ediff-split-window-function 'split-window-horizontally)
  (ediff-window-setup-function 'ediff-setup-windows-plain))

(use-package prog-mode
  :preface
  (defun da/prog-mode-setup ()
    (setq-local show-trailing-whitespace t)
    (display-line-numbers-mode 1)
    (subword-mode 1))
  :hook
  (prog-mode . da/prog-mode-setup))

(use-package magit
  :ensure t
  :bind ("C-x p m" . magit-project-status)
  :commands (magit-status magit-project-status)
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1)
  (magit-diff-paint-whitespace 'uncommitted)
  (magit-diff-highlight-trailing t)
  :config
  (transient-define-prefix gerrit-push ()
    "Push to a gerrit repository."
    :incompatible '(("wip" "ready"))
    ["Arguments"
     ("%w" "Work in progress" "wip")
     ("%r" "Ready" "ready")
     ("%t" gerrit-push:topic)]
    ["Push"
     ("g" gerrit-push-for-master)]
    (interactive)
    (transient-setup 'gerrit-push nil nil :scope (transient-args 'magit-push)))
  (defun gerrit-push-arguments ()
    (transient-args 'gerrit-push))
  (defun gerrit-push-git-flags ()
    (and transient-current-prefix
         (transient-scope)))

  (transient-define-argument gerrit-push:topic ()
    :description "Set topic"
    :class 'transient-option
    :argument "topic=")

  (transient-define-suffix gerrit-push-for-master (git-flags gerrit-args)
    "Push current head towards the explicit `HEAD:refs/for/master'"
    :description "origin HEAD:refs/for/master"
    (interactive (list (gerrit-push-git-flags) (gerrit-push-arguments)))
    (run-hooks 'magit-credential-hook)
    (let* ((ref-suffix (when gerrit-args (concat "%" (string-join gerrit-args ","))))
           (ref-specs (concat "refs/for/master" ref-suffix)))
      (magit-run-git-async "push" "-v" git-flags "origin" (concat "HEAD:" ref-specs))))

  (transient-insert-suffix 'magit-push "o"
    '("g" "gerrit" gerrit-push)))

(use-package diff-hl
  :ensure t
  :config
  (global-diff-hl-mode)
  (diff-hl-flydiff-mode))

(use-package eglot
  :preface
  (defun da/eglot-on-save-actions () (add-hook 'before-save-hook 'eglot-format-buffer -10 t))
  :custom
  (eglot-autoshutdown t)
  (eglot-events-buffer-size 0)
  (eglot-sync-connect nil)
  (eglot-ignored-server-capabilities '(:inlayHintProvider
                                       :documentHighlightProvider))
  :config
  (add-hook 'eglot-managed-mode-hook
            (lambda ()
              "Make sure Eldoc will show us all of the feedback at point."
              (setq-local eldoc-documentation-strategy
                          #'eldoc-documentation-compose)))
  (advice-add 'jsonrpc--log-event :override #'ignore)
  (add-to-list 'eglot-server-programs '(elixir-mode "elixir-ls"))
  (setq completion-category-defaults nil))

(use-package flymake
  :bind (:map flymake-mode-map
         ("M-n" . flymake-goto-next-error)
         ("M-p" . flymake-goto-prev-error)))

(use-package yasnippet
  :ensure t
  :bind (("C-<tab>" . yas-expand)
         :map yas-keymap
         ("M-n" . yas-next-field)
         ("M-p" . yas-prev-field)
         ("TAB" . nil)
         ("<tab>" . nil)
         ("<backtab>" . nil)
         ("S-<tab>" . nil))
  :config
  (yas-global-mode))

(use-package compile
  :bind (("C-c c" . compile))
  :custom
  (compile-command ""))

(use-package dape
  :ensure t)

;; Tree-sitter
(use-package treesit-auto
  :ensure t
  :custom
  (treesit-auto-install 'prompt)
  :config
  (treesit-auto-add-to-auto-mode-alist 'all)
  (global-treesit-auto-mode))


;; Rust
(use-package rust-ts-mode
  :hook ((rust-ts-mode . eglot-ensure)
         (rust-ts-mode . da/eglot-on-save-actions)))

(use-package cargo
  :ensure t
  :hook (rust-ts-mode . cargo-minor-mode))


;; C/C++
(defun da/cc-mode-setup ()
  (setq c-ts-mode-indent-style 'linux
        c-ts-mode-indent-offset 4))

(use-package c-ts-mode
  :hook ((c-ts-mode . eglot-ensure)
         (c-ts-mode . da/eglot-on-save-actions)))

(use-package c++-ts-mode
  :hook ((c++-ts-mode . eglot-ensure)
         (c++-ts-mode . da/eglot-on-save-actions)))


;; Elixir
(use-package elixir-ts-mode
  :hook ((elixir-ts-mode . eglot-ensure)
         (elixir-ts-mode . da/eglot-on-save-actions)))

(use-package mix
  :ensure t
  :hook (elixir-ts-mode . mix-minor-mode))


;; Scheme
(use-package geiser
  :ensure t)


;; Zig
(use-package zig-mode
  :ensure t
  :hook ((zig-mode . eglot-ensure)
         (zig-mode . da/eglot-on-save-actions)))


;; Web
(use-package plz
  :ensure t
  :commands (plz))

;; Python
(use-package python-ts-mode
  :preface
  (defun da/python-mode-setup ()
    (setq-local forward-sexp-function nil))
  :hook ((python-ts-mode . eglot-ensure)
         (python-ts-mode . da/python-mode-setup))
  :bind (:map python-ts-mode-map
              ("C-M-n" . python-nav-forward-sexp)
              ("C-M-p" . python-nav-backward-sexp)))


;; Yaml
(use-package yaml-ts-mode
  :ensure t
  :hook ((yaml-ts-mode . display-line-numbers-mode)))


;; Go
(use-package go-ts-mode
  :hook ((go-ts-mode . eglot-ensure)
         (go-ts-mode . da/eglot-on-save-actions))
  :custom
  (go-ts-mode-indent-offset 4))


;;; Tramp
(use-package tramp
  :config
  (add-to-list 'tramp-remote-path 'tramp-own-remote-path))


;;; Shell
;; Shell-mode
(use-package shell
  :bind (:map shell-mode-map
         ("M-r" . consult-history))
  :hook ((shell-mode . ansi-color-for-comint-mode-on))
  :config
  (add-to-list 'comint-output-filter-functions 'ansi-color-process-output))

;; Eshell-mode
(use-package eshell
  :preface
  (defun eshell-other-window (&optional arg)
    (interactive "P")
    (other-window-prefix)
    (eshell arg))

  :bind (("C-x e" . eshell)
         ("C-x 4 e" . eshell-other-window))
  :custom (eshell-visual-commands nil)
  :config
  (defun eshell/ff (&rest args)
    (apply #'find-file args))

  (defun eshell/ll (&rest args)
    (apply #'eshell/ls (append '("-l" "-a") args)))
  (add-to-list 'eshell-modules-list 'eshell-tramp)
  (add-to-list 'eshell-modules-list 'eshell-elecslash))

(use-package em-hist
  :hook ((kill-emacs . eshell-save-some-history))
  :bind (:map eshell-hist-mode-map
         ("M-s" . nil)
         ("M-r" . consult-history)))

(use-package em-prompt
  :bind (:map eshell-prompt-repeat-map
         ("n" . eshell-next-prompt)
         ("p" . eshell-previous-prompt)))


;; Terminal emulation
(use-package eat
  :ensure t
  :preface
  (defun da/eat-setup ()
    (setq-local scroll-conservatively 101))

  :commands (eat eat-project)
  :hook ((eat-mode . da/eat-setup)
         (eshell-load . eat-eshell-mode))

  :bind (:map eat-eshell-char-mode-map
         ("C-y" . eat-yank))

  :config
  (setq eat-term-name "xterm-256color") ; https://codeberg.org/akib/emacs-eat/issues/119
  )
